{*nginx configuration template file*}
server {
    listen ${LISTEN_PORT};
{*serving static file *}
    location /static {
        alias /vol/static;
    }

    location / {
        uwsgi_pass                ${APP_HOST}:${HOST_PORT};
        include                   /etc/nginx/uwsgi_params;
        clients_max_body_size     10M
    }

}



