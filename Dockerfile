FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="mzaman.aws@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

#DEFINE THE DEFAULT ENV VARIABLE FOR THE DOCKER CONTAINER
ENV LISTEN_PORT=8080
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

#CREATE SOME DIRECTORY AND CHANGE THE FILE PERMISSION DURING THE RUNTIME
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]

#
#COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
#COPY ./uwsgi_params /etc/nginx/uwsgi_params
#
#ENV LISTEN_PORT=8000
#ENV APP_HOST=app
#ENV APP_PORT=9000

#USER root

#RUN mkdir -p /vol/static
#RUN chmod 755 /vol/static
#RUN touch /etc/nginx/conf.d/default.conf
#RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
#
#COPY ./entrypoint.sh /entrypoint.sh
#RUN chmod +x /entrypoint.sh
#
#USER nginx
#
#CMD ["/entrypoint.sh"]
