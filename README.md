# recipe-app-api-proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on default (`8080`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default `9000`)    

## Setting up ECR repository to push the api proxy
* `set up IAM USER` - the IAM user will be used for GITLAB to push the image to the ECR
* `CREATE POLICTY` - create a policy and define minimum access rights for this policy: i.e. push to ECR
* `CREATE USER` - create a user and give it only "Programmatic access". This user will be used only for CI/CD tools.

## Setting up gitlab for CI/CD for the user
* `AWS ACCESS KEY ID AND SECRET ACCESS KEY` - Set up the access key id and secret in the variables.
* `Setup ECR REPO` - same way, but it takes the ECR Repo ARN 

